import java.net.InetSocketAddress
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel

class Server: Runnable {
    val selector = Selector.open()
    val serverChannel = ServerSocketChannel.open()

    init {
        serverChannel.socket().bind(InetSocketAddress(8083))
        serverChannel.configureBlocking(false)
        serverChannel.register(selector, SelectionKey.OP_ACCEPT)
    }

    override fun run() {
        selector.selectNow()

        for (key in selector.selectedKeys()) {
            if (!key.isValid) continue

            if (key.isAcceptable) {
                val client = serverChannel.socket() as SocketChannel

                client.configureBlocking(false)
                client.register(selector, SelectionKey.OP_READ)
            } else if (key.isReadable) {
                val client = key.channel() as SocketChannel
                var session = key.attachment() as Session?

                if (session == null) {
                    session = Session(client)
                    key.attach(session)
                }

                session.readData()

                val data = session.read()

                println(data)

                session.close()
            }
        }
    }

}