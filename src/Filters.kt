import com.example.*
import java.lang.StringBuilder
import java.time.Year
import java.util.*
import kotlin.collections.ArrayList


// TODO Переназвать методы
fun filter1(filters1: LinkedList<Pair<String, String>>): ArrayList<User> {
    val lim = filters1.find { it.first[0] == 'l' }
    var limit: Int = 0

    if (lim != null) {
        if (lim.second.toIntOrNull() != null)
            limit = lim.second.toInt()
        else return ArrayList()
    }

    // TODO ограницение размера после фильтрации
    val a = users.filter { isNeedUser(it.value, filters1) }.toList().map { it.second } as ArrayList<User>
    return a
}

fun filter(filters1: LinkedList<Pair<String, String>>): String {
    val lim = filters1.find { it.first[0] == 'l' }
    var limit: Int = 0

    if (lim != null) {
        if (lim.second.toIntOrNull() != null)
            limit = lim.second.toInt()
        else return ""
    }
    val a = users.filter { isNeedUser(it.value, filters1) }.toList().map { it.second } as ArrayList<User>
    return usersToJson(a, filters1, limit)
}

fun usersToJson(usersList: ArrayList<User>, filters: LinkedList<Pair<String, String>>, limit: Int = 0): String {
    val result = StringBuilder("{\"accounts\":[")

    var filters1 = filters.map {
        if (it.first.contains('_')) {
            it.first.substring(0, it.first.indexOf('_'))
        } else {
            it.first
        }
    }

    var i = 0
    val m = if(limit == 0) usersList.size else limit
    var res: ArrayList<String>

    usersList.take(m).forEach {
        val user = it
        res = ArrayList(m)
        result.append("{")
        res.add("\"id\":" + (user.id + 2))
        res.add("\"email\":\"" + emails[user.id] + "\"")
        filters1.forEach {
            when (it) {
                "sex" -> res.add("\"sex\":\"" + (if (user.sex == 1.toByte()) "m" else "f") + "\"")
                "city" -> if(user.city > 0) res.add("\"city\":\"" + cities[user.city] + "\"")
                "sname" -> if(snames[user.id] != null) res.add("\"sname\":\"" + snames[user.id] + "\"")
                "fname" -> res.add("\"fname\":\"" + fnames[user.id] + "\"")
                "phone" -> if (phones[user.id] != null) res.add("\"phone\":\"" + phones[user.id]!! + "\"")
                "birth" -> res.add("\"birth\":" + user.birth)
                "status" -> res.add("\"status\":\"" + (if (user.status == 1.toByte()) "свободны" else if (user.status == 3.toByte()) "всё сложно" else "заняты") + "\"")
                "joined" -> res.add("\"joined\":\"" + user.joined + "\"")
                "country" -> if(user.country > (0).toByte()) res.add("\"country\":\"" + countries[user.country] + "\"")
                "premium" -> if (user.premiumStart > 0) res.add("\"premium\":{\"finish\":" + user.premiumStart + ",\"start\":" + user.premiumEnd + "}")
                "interests" -> res.add("\"interests\":" + user.interests.map { it1 -> it1.toString() + " " + interests[it1] })
                "interests_any" -> res.add("\"interests\":" + user.interests.map { it1 -> it1.toString() + " " + interests[it1] })
                //"likes" -> res.add("\"likes\":[" + likes[user.id] + "]")
            }
        }
        i++

        result.append(res.joinToString(","))
        result.append("},")
    }

    if (usersList.size > 0) result.deleteCharAt(result.length - 1)

    result.append("]}")

    return result.toString()
}

fun isNeedUser(user: User, filters: LinkedList<Pair<String, String>>): Boolean {
        filters.forEach { filter ->
            when (filter.first) {
                "sex" -> if (!(if (filter.second == "m") user.sex == 1.toByte() else user.sex == 2.toByte())) { return false }
                "sex_eq" -> if (!(if (filter.second == "m") user.sex == 1.toByte() else user.sex == 2.toByte())) { return false }

                "email" -> if (emails[user.id] == null || emails[user.id] != filter.second) return false
                "email_domain" -> if (emails[user.id] == null || !emails[user.id]!!.endsWith(filter.second)) {
                        return false
                }

                "email_lt" -> if (emails[user.id] == null || (emails[user.id]!! > filter.second)) {
                        return false
                }

                "email_gt" -> if (emails[user.id] == null || (emails[user.id]!! < filter.second)) {
                        return false
                }

                "status_eq" -> {
                    if (user.status == 1.toByte()) {
                        if (filter.second[0] != 'с')
                            return false
                        else
                            println()
                    } else if (user.status == 2.toByte()) {
                        if (filter.second[0] != 'з')
                            return false
                        else
                            println()
                    } else if (user.status == 3.toByte()) {
                        if (filter.second[0] != 'в')
                            return false
                        else
                            println()
                    }
                }

                "status_neq" -> {
                    if (user.status == 1.toByte()) {
                        if (filter.second[0] == 'с') return false
                    } else if (user.status == 2.toByte()) {
                        if (filter.second[0] == 'з') return false
                    } else if (user.status == 3.toByte()) {
                        if (filter.second[0] == 'в') return false
                    }
                }

                "fname_eq" -> if (fnames[user.id] != null || (fnames[user.id] != filter.second)) {
                        return false
                }

                "fname_any" -> {
                    if (fnames[user.id] == null) {
                        return false
                    }

                    if (!filter.second.split(",").contains(fnames[user.id])) {
                        return false
                    }
                }

                "fname_null" -> {
                    if (filter.second == "0") {
                        if (fnames[user.id] == null) return false
                    }
                    else {
                        if (fnames[user.id] != null) return false
                    }
                }

                "sname_eq" -> if (snames.containsKey(user.id) || snames[user.id] != filter.second) {
                    return false
                }

                "sname_starts" -> {
                    val name = snames[user.id]
                    if (name != null) {
                        if (name.isNotEmpty()) {
                            if (!name.startsWith(filter.second)) {
                                return false
                            }
                        }
                        else return false
                    }
                    else return false
                }

                "sname_null" -> {
                    if (filter.second == "0") {
                        if (snames[user.id] == null) return false
                    }
                    else {
                        if (snames[user.id] != null) return false
                    }
                }

                "phone_code" -> if (!phones.containsKey(user.id) || phones[user.id]!!.substring(2..4) != filter.second) {
                    return false
                }

                "phone_null" -> {
                    if (filter.second == "1" && phones[user.id] != null) {
                        return false
                    } else if (filter.second == "0" && phones[user.id] == null) {
                        return false
                    }
                }

                "country_eq" -> {
                    if (countries[user.country] != null) {
                        if (countries[user.country] != filter.second) return false
                    } else return false
                }

                "country_null" -> if (!((filter.second == "0" && user.country > 0) ||
                    (filter.second == "1" && user.country == 0.toByte()))) {
                        return false
                }

                "city_eq" -> if (cities[user.city] != filter.second) {
                    return false
                }

                "city_any" -> if (!filter.second.split(",").contains(cities[user.city])) {
                    return false
                }

                "city_null" -> if ((filter.second == "1" && user.city > 0) ||
                    (filter.second == "0" && user.city == 0.toShort())) {
                        return false
                }

                "birth" -> if (user.birth / 31553280 + 1970 != Year.of(filter.second.toInt()).value) {
                    return false
                }

                "birth_gt" -> if (user.birth < filter.second.toInt()) {
                    return false
                }

                "birth_lt" -> if (user.birth > filter.second.toInt()) {
                    return false
                }

                "birth_year" -> if (user.birth / 31553280 + 1970 != Year.of(filter.second.toInt()).value) {
                    return false
                }

                "interests_contains" -> {
                    val inters = filter.second.split(',').map { it1 -> interestIds[it1] }

                    if (!(user.interests.containsAll(inters))) {
                        return false
                    }
                }

                "interests_any" -> {
                    val a = user.interests
                    val b = filter.second.split(',').map { it1 -> interestIds[it1] }

                    if(a.intersect(b).isEmpty())
                        return false
                }

                "likes" -> if (likes[user.id] == null || likes[user.id]!!.none { it1 -> it1.id == filter.second.toShort() }) return false
                "likes_contains" -> //if (!likes[user.id]!!.map { it.id }.containsAll(filter.second.split(',').map { it.toShort() })) {
                {
                    if (likes[user.id] != null ) {
                        val likes = likes[user.id]!!.map { it.id }
                        val filter1 = filter.second.split(",").map { it.toShort() }

                        if (!likes.containsAll(filter1)) return false
                    }
                    else return false
                }

                "premium_now" -> {
                        if (!(user.premiumStart > 1545354003 && user.premiumEnd < 1545354003)) {
                            return false
                        }
                }

                "premium_null" -> if (!((filter.second == "0" && user.premiumStart > 0) ||
                    (filter.second == "1" && user.premiumStart == 0))) {
                    return false
                }

                "joined" -> if(user.joined != filter.second.toInt()) return false
            }
        }

        return true
    }
