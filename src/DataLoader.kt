import com.example.mapUser
import com.example.parseJson
import com.example.users
import java.io.InputStreamReader
import java.nio.charset.Charset
import java.util.zip.ZipFile

fun loadData() {
    val file = ZipFile("/tmp/data/data.zip")
    val entries = file.entries().toList().reversed()

    for (entry in entries) {
        if (entry.name.startsWith("accounts")) {
            val stream = file.getInputStream(entry)
            readFile(InputStreamReader(stream, Charset.forName("UTF-8")))
        }

        println(entry.name)
    }
    System.gc()
}

fun readFile(inputStream: InputStreamReader) {
    val string = StringBuilder()
    var data = inputStream.read()
    var brackets = 0
    var start = 0

    while (start < 14) {
        inputStream.read()
        start += 1
    }

    while (data != -1) {
        val char = data.toChar()

        when (char) {
            '{' -> brackets += 1
            '}' -> brackets -= 1
            else -> {
                if (brackets == 0) {
                    val userData = parseJson(string.toString())

                    if (userData != null && userData.size > 0) {
                        val user = mapUser(userData)

                        if (user != null) users.put((users.size - 1).toShort(), user)
                        string.clear()
                    }
                }
            }
        }

        string.append(char)
        data = inputStream.read()
    }
}