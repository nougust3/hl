import com.example.*
import kotlin.collections.ArrayList

fun validateEmail(email: String): Boolean =
    email.length in 6..100

fun validateName(name: String): Boolean =
    name.length < 50

fun validatePhone(phone: String): Boolean =
    phone.length < 16

fun validateSex(sex: String): Boolean =
    sex == "m" || sex == "f"

fun validateBirth(birth: Int): Boolean =
    birth in -631152000..1104537600

fun validateCountry(country: String): Boolean =
    country.length < 50

fun validateCity(city: String): Boolean =
    city.length < 50

fun validateJoined(joined: Int): Boolean = true
    //joined in 1293840001..1514764799

fun validateStatus(status: String): Boolean =
    status == "свободны" || status == "заняты" || status == "всё сложно"

fun validateInterests(interests: ArrayList<String>): Boolean =
    !interests.any { it.length > 100 }

fun validatePremium(premiumStart: Int, premiumFinish: Int): Boolean =
    premiumStart > 1514764800 && premiumFinish > 1514764800

fun validateLikes(likes: ArrayList<Like>): Boolean = true
    //!likes.any { validateBirth(it.date) }