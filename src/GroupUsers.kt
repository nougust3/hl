import com.example.*
import filter1
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun getGroupName(user: User, keys: ArrayList<String>): String {
    var result = ""

    keys.forEach { key ->
        when (key) {
            "status" -> result += user.status.toString() + ","
            "sex" -> result += user.sex.toString() + ","
            "interests" -> result += user.interests.toString() + ","
            "country" -> result += user.country.toString() + ","
            "city" -> result += user.city.toString() + ","
        }
    }

    return result
}

fun buildMap(filteredUsers: ArrayList<User>, keys: ArrayList<String>, order: Int, limit: Int): String {
    val result = HashMap<String, Short>()

    filteredUsers.forEach { user ->
        val group = getGroupName(user, keys)

        result[group] = if (result[group] == null) 1 else (result[group]!! + 1).toShort()
    }

    return sortMap(result, keys, order, limit)
}

fun sortMap(groups: HashMap<String, Short>, keys: ArrayList<String>, order: Int, limit: Int): String {
    return printMap(groups.entries
        .sortedBy { it.value * order }
        .associateBy({ it.key }, { it.value })
            as HashMap<String, Short>, keys, limit)
}

fun printMap(groups: HashMap<String, Short>, keys: ArrayList<String>, limit: Int): String {
    val result = StringBuilder("{\"groups\":[")
    var count = 0

    groups.forEach {
        val names = it.key.split(",")

        if (count == limit) return@forEach

        result.append("{")
        for (i in 0 until keys.size) {
            when (keys[i]) {
                "status" -> {
                    result.append("\"status\":")
                    when (names[i]) {
                        "0" -> result.append("\"не указан\",")
                        "1" -> result.append("\"свободны\",")
                        "2" -> result.append("\"заняты\",")
                        "3" -> result.append("\"всё сложно\",")
                    }
                }
                "sex" -> result.append("\"sex\":").append(if (names[i] == "1") "\"m\"," else "\"f\",")
                /*"city" -> if(cities[i].isNotEmpty() && countries[names[i].toByte()] != null) result.append("\"city\":").append("\"" + cities[names[i].toShort()] + "\",")
                "country" -> if(names[i].isNotEmpty() && countries[names[i].toByte()] != null) result.append("\"country\":").append("\"" + countries[names[i].toByte()] + "\",")
                "interest" -> if(names[i].isNotEmpty() && countries[names[i].toByte()] != null) result.append("\"interest\":").append("\"" + interests[names[i].toInt()] + "\",")
            */}
        }

        result.append("\"count\":" + it.value+"}")
        if (count < groups.size) result.append(",")
        count +=1

    }


    result.deleteCharAt(result.length - 1)
    result.append("]}")
    return result.toString()
}

fun groupUsers(params: LinkedList<Pair<String, String>>): String {
    val keys = ArrayList<String>()
    var limit = 50
    var order = 1

    params.forEach {
        if (it.first[0] == 'o' && it.second[0] == '-') order = -1
        else if (it.first[0] == 'l') {
            if (!it.second[0].isDigit()) return ""
            limit = it.second.toInt()
        }
        else if (it.first[0] == 'k') keys.addAll(it.second.split(','))
    }

    val result = filter1(params)

    if (result.isEmpty()) return ""

    return buildMap(filter1(params), keys, order, limit)
}