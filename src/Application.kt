package com.example
import filter
import groupUsers
import loadData
import org.apache.commons.text.StringEscapeUtils
import recommend
import validateBirth
import validateCity
import validateCountry
import validateEmail
import validateJoined
import validateLikes
import validateName
import validatePhone
import validateSex
import validateStatus
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.ServerSocket
import java.net.Socket
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.collections.ArrayList
import com.sun.xml.internal.ws.streaming.XMLStreamReaderUtil.close
import com.sun.xml.internal.ws.streaming.XMLStreamWriterUtil.getOutputStream
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel


class Like(var date: Int, var id: Short)

class User {
    var id: Short = 0
    var interests: ArrayList<Int> = ArrayList()
    var status: Byte = 0
    var premiumStart: Int = 0
    var premiumEnd: Int = 0
    var sex: Byte = 0
    var birth: Int = 0
    var city: Short = 0
    var country: Byte = 0
    var joined: Int = 0
}

var users = Hashtable<Short, User>(30010)
val cities = Hashtable<Short, String>(1000)
val countries = Hashtable<Byte, String>(100)
val interests = Hashtable<Int, String>(1000)
val fnames = Hashtable<Short, String>(30000)
val snames = Hashtable<Short, String>(30000)
val emails = Hashtable<Short, String>(30010)
val phones = Hashtable<Short, String>(30010)
val likes = Hashtable<Short, ArrayList<Like>>(10010)
val cityIds = Hashtable<String, Short>(1000)
val countryIds = Hashtable<String, Byte>(100)
val interestIds = Hashtable<String, Int>(1000)

val allowedParams = arrayOf("premium_null", "premium_now", "likes_contains", "interests_any",
    "interests_contains", "birth_year", "birth_gt", "birth_lt", "city_null", "city_any", "city_eq",
    "country_null", "phone_null", "country_eq", "phone_code", "sname_null", "sname_null",
    "sname_starts", "sname_eq", "fname_null", "fname_any", "fname_eq", "status_neq",
    "status_eq", "email_gt", "email_lt", "email_domain", "sex_eq", "limit", "query_id")

fun run(request: String): String {
    var req = ""
    var method = 0
    var id = 0

    if (request[0] == 'G') {
        if (request[14] == 'f') {
            req = request.substring(22, request.length - 9)
            method = 1
        } else if (request[14] == 'g') {
            req = request.substring(21, request.length - 9)
            method = 2
        } else for (i in 14..21) {
            if (!request[i].isDigit()) {
                if (i == 14) {
                    req = BAD_REQUEST; break
                }

                id = request.substring(14, i).toInt()

                req = when {
                    request[i + 1] == 'r' -> {
                        method = 3
                        request.substring(i + 12, request.length - 9)
                    }
                    request[i + 1] == 's' -> {
                        method = 4
                        request.substring(i + 10, request.length - 9)
                    }
                    else -> ""
                }

                break
            }
        }
    }

    val params = LinkedList<Pair<String, String>>()

    for (param in req.split('&')) {
        if (param[0] == 'q') continue

        val s = param.indexOf('=')

        if (s < 0 || s + 1 == param.length) {
            req = BAD_REQUEST; break
        }

            params.add(
                Pair(
                    param.substring(0, s),
                    URLDecoder.decode(param.substring(s + 1, param.length), "UTF-8")
                )
            )
    }

    when (method) {
        1 -> req = filter(params)
        2 -> req = groupUsers(params)
        3 -> req = ""//recommend(id, params)
        4 -> req = suggest(id, 0)
    }

    return req
}

val address = InetSocketAddress(8083)

class Server: Runnable {
    var i = 0
    val selector = Selector.open()
    val serverChannel = ServerSocketChannel.open()

    init {
        serverChannel.socket().bind(address)
        serverChannel.configureBlocking(false)
        serverChannel.register(selector, SelectionKey.OP_ACCEPT)
    }

    override fun run() {
        selector.selectNow()

        keys@for (key in selector.selectedKeys()) {
            if (!key.isValid) continue

            if (key.isAcceptable) {
                val client: SocketChannel? = serverChannel.accept()

                if (client != null) {
                    client.configureBlocking(false)
                    client.register(selector, SelectionKey.OP_READ)
                }
            } else if (key.isReadable) {
                val client = key.channel() as SocketChannel
                var session = key.attachment() as Session?

                if (session == null) {
                    session = Session(client)
                    key.attach(session)
                }

                session.readData()

                val data = session.read()

                if (data != null && i == 5) {
                    println(data)
                    val result = respond(com.example.run(data))

                    i = 0
                    session.send(result)
                    break@keys
                }
                session.close()
                i += 1
            }
        }
    }
}

class Session(val channel: SocketChannel) {

    private val buffer = ByteBuffer.allocate(2048)
    private val lines = StringBuilder()
    private var mark = 0

    fun read(): String? {
        val result = StringBuilder()
        var lastChar = -1

        while (buffer.hasRemaining()) {
            val char = buffer.get().toChar()

            result.append(char)

            if (char == '\n' && lastChar.toChar() == '\r') {
                mark = buffer.position()
                lines.append(result)

                return result.substring(0, result.length - 2)
            }

            lastChar = char.toByte().toInt()
        }

        return null
    }

    fun readData() {
        buffer.limit(buffer.capacity())

        val read = channel.read(buffer)

        if (read == -1) return

        buffer.flip()
        buffer.position(mark)
    }

    fun send(data: String) {
        val bytes = data.toByteArray(Charsets.UTF_8)
        channel.write(ByteBuffer.wrap(bytes))
    }

    fun close() {
        channel.close()
    }
}


fun main(args: Array<String>) {
    loadData()

    println(usersToJson2(users.filter { emails[it.value.id] == "hulpotawerannetivso@yandex.ru" }.map { it.value }.toList()))
    println("done")
    val server = Server()

    while (true) {
        server.run()
    }
}

fun respond(data: String): String {
    return when (data) {
        "" -> "HTTP/1.1 400 Bad Request\n" +
                "Server: oIo\n" +
                "Content-Type: text/html; charset=utf-8\n" +
                "Connection: keep-alive"

        " " -> "HTTP/1.1 404 Not Found\n" +
                "Server: oIo\n" +
                "Content-Type: text/html; charset=utf-8\n" +
                "Connection: keep-alive"

        else -> "HTTP/1.1 200 Ok\n" +
                "Server: oIo\n" +
                "Content-Length: " + data.length + "\n" +
                "Content-Type: text/html; charset=utf-8\n" +
                "Connection: keep-alive\r\n\r\n" + data
    }
}

const val BAD_REQUEST = "HTTP/1.1 400 Bad Request\n" +
        "Server: oIo\n" +
        "Content-Length: 230\n" +
        "Content-Type: text/html; charset=utf-8\n" +
        "Connection: Closed"

fun usersToJson2(usersList: List<User>): String {
    val result = StringBuilder("[\n")

    usersList.forEach { user ->
        result.append("\t{\n")
        result.append("\t\t\"sex\": \"" + (if (user.sex == 1.toByte()) "m" else "f") + "\",\n")
        result.append("\t\t\"id\": \"" + user.id + "\",\n")
        result.append("\t\t\"city\": \"" + cities[user.city] + "\",\n")
        result.append("\t\t\"sname\": \"" + snames[user.id] + "\",\n")
        result.append("\t\t\"fname\": \"" + fnames[user.id] + "\",\n")
        if (phones[user.id] != null) result.append("\t\t\"phone\": \"" + phones[user.id]!! + "\",\n")
        result.append("\t\t\"birth\": \"" + user.birth + "\",\n")
        result.append("\t\t\"email\": \"" + emails[user.id] + "\",\n")
        result.append("\t\t\"sex\": \"" + if (user.sex == 1.toByte()) "m" else "f" + "\",\n")
        result.append("\t\t\"joined\": \"" + user.joined + "\",\n")
        result.append("\t\t\"country\": \"" + countries[user.country] + "\",\n")
        result.append("\t\t\"premium\": {\"finish\": " + user.premiumStart + ", \"start\": " + user.premiumEnd + "\"},\n")
        result.append("\t\t\"interests\": " + user.interests.map { interests[it] } + ",\n")
        result.append("\t\t\"likes\": [" + likes[user.id] + "],\n")
        result.append("\t},\n")
    }

    result.append("]")

    return result.toString()
}
/*
fun Application.main() {
    loadData()

        routing {
            get("/accounts/filter/") {
                val filters = ArrayList<Pair<String, String>>()
                val filterNames = ArrayList<String>()

                call.request.queryParameters.forEach { s, list ->
                    filterNames.add(s.takeWhile { c -> c != '_' })
                    filters.add(Pair(s, list[0]))
                }

                if (call.request.queryParameters.toMap().map { s -> s.key}.intersect(allowedParams.toList()).size != filterNames.size) {
                    call.respond(HttpStatusCode.BadRequest, "")
                }else {

                    val limit = filters.filter { it1 -> it1.first == "limit" }
                    var limit1 = 0
                    if (limit.isNotEmpty()) {
                        limit1 = if (limit[0].second[0].isDigit()) limit[0].second.toInt() else 0
                    }
                    else {
                        call.respond(HttpStatusCode.BadRequest, "")
                        return@get
                    }

                    if (filters.size > 0) {
                        val usersList = filter(filters)

                        call.respond(
                            HttpStatusCode.OK, usersToJson(
                                usersList as ArrayList<User>,
                                filterNames, limit1
                            )
                        )
                    } else call.respond(HttpStatusCode.BadRequest, "")
                }
            }

            get("/accounts/group") {
                /*if (call.request.queryParameters.toMap().map { s -> s.key}.intersect(allowedParams2.toList()).size != call.request.queryParameters.toMap().size) {
                    call.respond(HttpStatusCode.BadRequest, "")
                }*/
                //else c
                call.respond(HttpStatusCode.OK, groupUsers(call.request.queryParameters))
            }

            get("/accounts/{id}/recommend") {
                val idParam = call.parameters["id"]

                if (idParam == null) call.respond(HttpStatusCode.NotFound, "")
                else {
                    val id = idParam.toInt() - 2

                    if (id > users.size) call.respond(HttpStatusCode.NotFound, "")
                    else {


                        val limit = if (call.parameters["limit"] != null) call.parameters["limit"]!!.toInt() else 50

                        call.respond(HttpStatusCode.OK, users[id.toShort()]!!.interests.toString() + "\n" + suggest(id, limit))
                    }
                }
            }

            get("/accounts/{id}/suggest") {
                var limit = 0
                var country = 0
                var city = 0

                if (call.parameters["limit"] != null) {
                    limit = call.parameters["limit"]!!.toInt()
                }

                if (call.parameters["country"] != null && countryIds[call.parameters["country"]!!] == null) {
                    call.respond(HttpStatusCode.NotFound, "")
                } else {
                    if (call.parameters["country"] != null) {
                        country = countryIds[call.parameters["country"]!!]!!.toInt()
                    }
                }

                if (call.parameters["city"] != null && cityIds[call.parameters["city"]!!] == null) {
                    call.respond(HttpStatusCode.NotFound, "")
                } else {
                    if (call.parameters["city"] != null) {
                        city = cityIds[call.parameters["city"]!!]!!.toInt()
                    }
                }

                var result = recommend((call.parameters["id"]!!.toInt() - 2).toShort(), limit, country, city)

                if (result.length < 17) {
                    call.respond(HttpStatusCode.NotFound, "")
                } else {
                    call.respond(HttpStatusCode.OK, result)
                }
            }

            get("/users") {
                val filters = ArrayList<Pair<String, String>>()
                val filterNames = ArrayList<String>()

                call.request.queryParameters.forEach { s, list ->
                    filterNames.add(s.takeWhile { c -> c != '_' })
                    filters.add(Pair(s, list[0]))
                }
                call.respond(HttpStatusCode.OK, usersToJson(users))
            }

            post("/accounts/<id>/") {

            }

            get("/accounts/new/") {
                val data = parseJson(call.request.toString())

                if (data == null) {
                    call.respond(HttpStatusCode.BadRequest, "")
                    return@get
                }

                val user = mapUser(data)

                if (user != null) {
                    users[users.size.toShort()] = user
                    call.respond(HttpStatusCode.Created, user.toString())
                } else call.respond(HttpStatusCode.BadRequest, "")
            }

            post("/accounts/likes/") {

            }
        }
}*/

fun parseJson(json: String): ArrayList<Pair<String, String>>? {
    val result = ArrayList<Pair<String, String>>()
    val nextFilter = StringBuilder()
    val nextValue = StringBuilder()
    var currentChar: Char
    var isNumber = false
    var isArray = false
    var isObject = false
    var state = FIND_FILTER

    for (i in 0 until json.length) {
        currentChar = json[i]

        when (state) {
            FIND_FILTER -> if (currentChar == QUOTE) state = GET_FILTER
            GET_FILTER -> if (currentChar == QUOTE) state = FIND_VALUE
                else nextFilter.append(currentChar)
            FIND_VALUE -> when (currentChar) {
                QUOTE -> state = GET_VALUE
                in '0'..'9' -> {
                    isNumber = true
                    nextValue.append(currentChar)
                    state = GET_VALUE
                }
                '[' -> {
                    isArray = true
                    state = GET_VALUE
                }
                '{' -> {
                    isObject = true
                    state = GET_VALUE
                }
            }
            GET_VALUE ->
                if (isNumber && currentChar in '0'..'9') nextValue.append(currentChar)
                else if (
                    (!isObject && !isArray && !isNumber && currentChar == QUOTE) ||
                    (isNumber && currentChar !in '0'..'9') ||
                    (isArray && currentChar == ']') ||
                    (isObject && currentChar == '}')
                ) {
                    state = FIND_FILTER
                    if (isArray || isObject) {
                        val a = nextFilter.toString()
                        val b = nextValue.toString()
                        if (a.isNotEmpty() && b.isNotEmpty())
                        result.add(Pair(a, b))
                    }
                    else result.add(Pair(nextFilter.toString(), nextValue.toString()))

                    isNumber = false
                    isArray = false
                    isObject = false

                    nextFilter.clear()
                    nextValue.clear()
                } else nextValue.append(currentChar)
        }
    }

    return result
}

fun mapUser(data: ArrayList<Pair<String, String>>): User? {
    val user = User()
    user.id = (users.size -1).toShort()

    data.forEach {
        // TODO Проверка экранированости строки числом
        val value = if (it.second.startsWith("\\")) StringEscapeUtils.unescapeJava(it.second) else it.second
        when (it.first) {
            "fname" -> if (validateName(value)) fnames[user.id] = value else return null
            "sname" -> if (validateName(value)) snames[user.id] = value else return null
            "email" -> if (validateEmail(value)) emails[user.id] = value else return null
            "status" -> if (validateStatus(value)) {
                user.status = when (value) {
                    "свободны" -> 1
                    "заняты" -> 2
                    "всё сложно" -> 3
                    else -> 0
                }
            } else return null
            "phone" -> if (validatePhone(value)) {
                phones[user.id] = value
            } else return null
            "birth" -> {
                val birth = value.toIntOrNull()
                if (birth != null && validateBirth(birth)) user.birth = birth else return null
            }
            "joined" -> {
                val joined = value.toIntOrNull()
                if (joined != null && validateJoined(joined)) user.joined = joined else return null
            }
            "city" -> {
                if (validateCity(value)) {
                    val id = cityIds[value]

                    if (id != null) user.city = id
                    else {
                        val cityId = (cities.size + 1).toShort()
                        cities[cityId] = value
                        cityIds[value] = cityId
                        user.city = cityId
                    }
                } else return null
            }
            "country" -> {
                if (validateCountry(value)) {
                    val id = countryIds[value]

                    if (id != null) user.country = id
                    else {
                        val newId = (countries.size + 2).toByte()
                        countries[newId] = value
                        countryIds[value] = newId
                        user.country = newId
                    }
                } else return null
            }
            "interests" -> {
                if (value.isNotEmpty()) {
                    val interestsData = ArrayList(value.split(", "))

                    // TODO Проверка валидности интересов
                    //if (validateInterests(interestsData)) {
                        val list = ArrayList<Int>()

                        interestsData.forEach { interest ->
                            val inter = interest.substring(1..interest.length-2)
                            val interestName = if (inter.startsWith("\\")) StringEscapeUtils.unescapeJava(inter) else inter
                            val id = interestIds[interestName]

                            if (id != null) list.add(id)
                            else {
                                val interestId = interests.size + 1
                                interests[interestId] = interestName
                                interestIds[interestName] = interestId
                                list.add(interestId)
                            }
                        }

                        user.interests = list
                    //}
                    //else return null
                }
            }
            "sex" -> if (validateSex(value)) {
                user.sex = when (value) {
                    "m" -> 1
                    "f" -> 2
                    else -> 0
                }
            } else return null
            "premium" -> if (value.isNotEmpty()) {
                val a = value.split(',').map { it1 -> it1.split(": ") }
                if(a[0][0][1] == 'f') {
                    user.premiumEnd = a[1][1].toInt()
                    user.premiumStart = a[0][1].toInt()
                }
                else  {
                    user.premiumEnd = a[0][1].toInt()
                    user.premiumStart = a[1][1].toInt()
                }
            }
            "likes" -> if (value.isNotEmpty()) {
                val likesData = value.split("},").toList()

                val dt = StringBuilder()
                val id = StringBuilder()
                var state = 0
                val likes1 = ArrayList<Like>()

                likesData.forEach { like ->
                    val likeData = like.split(",")

                    likeData.forEach { it1 ->
                        for (i in 0 until it1.length) {
                            val c = it1[i]
                            when (state) {
                                0 -> {
                                    if (c == 't') state = 1
                                    else if (c == 'i') state = 2
                                }
                                1 -> {
                                    if (c in '0'..'9') {
                                        dt.append(c)
                                        state = 3
                                    }
                                }
                                2 -> {
                                    if (c in '0'..'9') {
                                        id.append(c)
                                        state = 4
                                    }
                                }
                                3 -> {
                                    if (c in '0'..'9') dt.append(c)
                                    else state = 2
                                }
                                4 -> {
                                    if (c in '0'..'9') id.append(c)
                                    else state = 1
                                }
                            }
                        }

                    }

                    likes1.add(Like(dt.toString().toInt(), id.toString().toShort()))
                    dt.clear()
                    id.clear()
                    state = 0
                }

                // TODO Реализовать проверку валидности лайков
                if (validateLikes(likes1)) {
                    likes[user.id] = likes1
                }
            }
        }
    }

    return user
}

