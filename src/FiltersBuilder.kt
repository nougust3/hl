package com.example

import java.lang.StringBuilder

const val GET_FILTER = 1
const val FIND_FILTER = 2
const val GET_VALUE = 3
const val FIND_VALUE = 4

const val QUOTE = '"'

fun buildFilters(request: String): List<Pair<String, String>>? {
    if (request.isEmpty()) return null

    val result = ArrayList<Pair<String, String>>()
    val nextFilter = StringBuilder()
    val nextValue = StringBuilder()
    var currentChar: Char
    var state = FIND_FILTER

    for (i in 0 until request.length) {
        currentChar = request[i]

        when (state) {
            FIND_FILTER -> if (currentChar == QUOTE) state = GET_FILTER
            GET_FILTER ->
                if (currentChar == QUOTE) state = FIND_VALUE
                else nextFilter.append(currentChar)
            FIND_VALUE -> if (currentChar == QUOTE) state = GET_VALUE
            GET_VALUE ->
                if (currentChar == QUOTE) {
                    state = FIND_FILTER
                    val value = nextValue.toString()
                    //if (nextFilter.isNotEmpty() && nextValue.isNotEmpty() && isValidFilter(value))
                        result.add(Pair(nextFilter.toString(), nextValue.toString()))
                    //else return null
                    nextFilter.clear()
                    nextValue.clear()
                } else nextValue.append(currentChar)
        }
    }

    if (result.size == 0)
        return null

    return result
}

val validFilterNames = arrayOf("fname", "sname", "email", "city", "country", "interests",
    "birth", "id", "sex", "likes", "premium", "status", "joined")

fun isValidFilter(name: String): Boolean =
    validFilterNames.contains(name)
