import java.nio.ByteBuffer
import java.nio.channels.SelectableChannel
import java.nio.channels.SocketChannel

class Session(val channel: SocketChannel) {

    private val buffer = ByteBuffer.allocate(2048)
    private val lines = StringBuilder()
    private var mark = 0

    fun read(): String? {
        val result = StringBuilder()
        var lastChar = -1

        while (buffer.hasRemaining()) {
            val char = buffer.get().toChar()

            result.append(char)

            if (char == '\n' && lastChar.toChar() == '\r') {
                mark = buffer.position()
                lines.append(result)

                return result.substring(0, result.length - 2)
            }

            lastChar = char.toByte().toInt()
        }

        return null
    }

    fun readData() {
        buffer.limit(buffer.capacity())

        val read = channel.read(buffer)

        if (read == -1) return

        buffer.flip()
        buffer.position(mark)
    }

    fun close() {
        channel.close()
    }
}
