package com.example


fun suggest(userId: Int, limit: Int): String {
    if (users[userId.toShort()] == null) return " "
    val result = StringBuilder()
    val user = users[userId.toShort()]!!
    val userInterests = user.interests
    val userInterestsCount = userInterests.size

    val usersForCheck = ArrayList<Pair<User, Float>>()

    users.entries.forEach {
        val interestCount = it.value.interests.intersect(userInterests).size
        val statusScore: Float = 1f / it.value.status
        val interestScore: Float = userInterestsCount.toFloat() / interestCount
        val ageScore: Float = Math.max(user.birth, it.value.birth).toFloat() / Math.min(user.birth, it.value.birth)
        val totalScore: Float = statusScore * interestScore * ageScore

        usersForCheck.add(Pair(it.value, totalScore))
    }

    usersForCheck.sortBy { it.second * (-1) }

    result.append("{\"accounts\":[")

    usersForCheck.take(10).forEach {
        val u = it.first
        result.append("{")
        result.append("\"id\":" + u.id + ",")
        result.append("\"email\":" + emails[u.id] + ",")
        result.append("\"status\":" + u.status + ",")
        result.append("\"fname\":" + fnames[u.id] + ",")
        result.append("\"sname\":" + snames[u.id] + ",")
        result.append("\"birth\":" + u.birth + ",")
        result.append("\"interests\":" + u.interests.toString() + ",")
        result.append("\"premium\":" + u.premiumStart + "," + u.premiumEnd + ",")
        result.append("}," + u.interests.intersect(userInterests).size + " " + it.second + "\n")
    }

    result.append("]")

    return result.toString()
}