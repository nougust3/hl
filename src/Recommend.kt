import com.example.*
import java.lang.StringBuilder
import java.util.*

fun getScore(likes1: List<Like>, likes2: List<Like>): Int {
    var score = 0

    if (likes1.size >= likes2.size)
        for (i in 0 until likes1.size)
            for (j in 0 until likes2.size)
                if (likes1[i].id == likes2[j].id)
                    score += 1000000000 / (1000000000 - Math.abs(likes1[i].date - likes2[j].date) + 1)
    else for (m in 0 until likes2.size)
        for (n in 0 until likes1.size)
            if (likes1[n].id == likes2[m].id)
                score += 1000000000 / (1000000000 - Math.abs(likes1[n].date - likes2[m].date))

    return score
}

fun recommend(id: Int, params: LinkedList<Pair<String, String>>): String {
    val currentUser = users[id.toShort()] ?: return ""
    val currentLikes = likes[id.toShort()] ?: return ""
    val usersForCheck = ArrayList<Pair<Short, Int>>()
    val result = StringBuilder("{\"accounts\":[")
    val suggestedUsers = ArrayList<Short>()
    var limit = 0
    var country: Byte = 0
    var city: Short = 0
    var suggestedCount = 0
    val zeroByte: Byte = 0
    val zeroShort: Short = 0

    params.forEach {
        if (it.first[0] == 'c' && it.first[1] == 'i') city = cityIds[it.second] ?: 0
        else if (it.first[0] == 'c' && it.first[1] == 'o') country = countryIds[it.second] ?: 0
        else if (it.first[0] == 'l') {
            if (!it.second[0].isDigit()) return ""
            limit = it.second.toInt()
        }
    }

    usersLoop@for (i in 0 until users.size) {
        val targetUser = users[i.toShort()] ?: continue@usersLoop

        if (targetUser.sex == currentUser.sex &&
            (country == zeroByte || targetUser.country == country) &&
            (city == zeroShort || targetUser.city == city)
        ) {
            val targetLikes = likes[targetUser.id] ?: continue@usersLoop
            val s = getScore(currentLikes, targetLikes)
            if (s > 0) usersForCheck.add(Pair(targetUser.id, s))
        }
    }

    usersForCheck
        .sortedBy { it.second * -1 }
        .forEach {
            if (suggestedCount < limit) return@forEach
            if (limit - suggestedCount > likes[it.first]!!.size)
                likes[it.first]!!.forEach { like -> if (suggestedUsers.size < limit) {
                    suggestedCount += 1
                    suggestedUsers.add(like.id)
                } }
            else likes[it.first]!!.sortedBy { like -> like.id * -1 }
                .forEach { like -> if (suggestedUsers.size < limit) {
                    suggestedCount += 1
                    suggestedUsers.add(like.id)
                } }
        }

    suggestedUsers.forEach {
        val user = users[(it - 2).toShort()]!!
        result.append("{\"email\":\"").append(emails[user.id]!!).append("\",\"id\":").append((user.id + 2)).append(",")
        if (fnames[user.id] != null) result.append("\"fname\":" + "\"" + fnames[user.id]!! + "\",")
        if (snames[user.id] != null) result.append("\"sname\":" + "\"" + snames[user.id]!! + "\",")
        result.append("\"status\":\"").append((if (user.status == 1.toByte()) "свободны" else if (user.status == 3.toByte()) "всё сложно" else "заняты")).append("\"},")
    }

    result.deleteCharAt(result.length - 1)
    result.append("]}")

    return result.toString()
}
